import './App.css';
import { ThemeProvider, CssBaseline } from '@mui/material'
import customtheme from './assets/theme'

//router
import { BrowserRouter as Router, Route, Routes, Link } from "react-router-dom";

//context
import { ProtocolProvider } from "./context/ProtocolContext"

//pages
import Home from "./pages/index"


function App() {
  return (
    <div className="App">
      <ThemeProvider theme={customtheme}>
      <ProtocolProvider>
      <CssBaseline />
      <Router>
        <Routes>
          <Route path='/' element={<Home />} />
        </Routes>
      </Router>
      </ProtocolProvider>
      </ThemeProvider>
    </div>
  );
}

export default App;