import { createTheme } from '@mui/material/styles';

const primaryColor = "#32CD32"
const secondaryColor = "#32CD32"

const theme = createTheme({
  palette: {
    mode: "dark",
    
    primary: {
      main: primaryColor,
    },
    secondary: {
      main: secondaryColor,
    },
  },

  components: {
    MuiButton: {
    styleOverrides: {
      text: {
        textTransform: 'none',
        '&:hover' :{
          color: primaryColor,
        }
      },
    },
  },
},
});



export default theme;