import React, { useState, useEffect, useContext } from "react";

// styling imports
import Paper from "@mui/material/Paper";
import Box from "@mui/material/Box";

// import components
import LockStepper from "./LockStepper.js";

const LockArea = () => {

  return (
    <Box
      sx={{
        width: "100%",
        pr: "12px",
        pl: "10px",
      }}
    >
      <Paper elevation={1} sx={{ height: 320, borderRadius: '16px' }}>
        <Box sx={{ pl: "20px", pr: "20px", pt: "20px", pb: "20px" }}>
        <LockStepper />
        <Box>
          </Box>
        </Box>
      </Paper>
    </Box>
  );
};

export default LockArea;