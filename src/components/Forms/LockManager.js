import React, { useState, useEffect, useContext } from "react";
import Box from "@mui/material/Box";
import Typography from "@mui/material/Typography";
import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableCell from "@mui/material/TableCell";
import TableContainer from "@mui/material/TableContainer";
import TableHead from "@mui/material/TableHead";
import TableRow from "@mui/material/TableRow";
import Pagination from "@mui/material/Pagination";

// import context
import { ProtocolContext } from "../../context/ProtocolContext.js";

export default function LockManager() {

  const { allUserLocks, getLockInfo, unlockTimes } = useContext(ProtocolContext);

  useEffect(() => {
    getLockInfo();
  }, []);

  useEffect(() => {
  getLockInfo();
  }, [allUserLocks.length]);

  // Pagination settings
  const itemsPerPage = 6; // Number of items per page
  const [page, setPage] = useState(1);

  // Function to handle page change
  const handlePageChange = (event, value) => {
    setPage(value);
  };

  // Slice the data based on the current page
  const startIndex = (page - 1) * itemsPerPage;
  const endIndex = startIndex + itemsPerPage;
  const locksToShow = allUserLocks ? allUserLocks.slice(startIndex, endIndex) : [];
  const unlockTimesToShow = unlockTimes ? unlockTimes.slice(startIndex, endIndex) : [];

  const totalPages = Math.ceil((allUserLocks?.length || 0) / itemsPerPage); // Handle undefined allUserLocks

  return (
    <Box sx={{ maxWidth: 400 }}>
      
      {locksToShow.length > 0 ? (
        <React.Fragment>
          <TableContainer
            sx={{
              minHeight: "250px",
              maxHeight: "250px",
              minWidth: "350px",
              maxWidth: "250px",
            }}
          >
            <Table>
              <TableHead>
                <TableRow>
                  <TableCell
                    sx={{
                      fontSize: "14px",
                      pt: "0px", // Adjust cell padding
                      pl: "8px", // Adjust cell padding
                      pr: "8px", // Adjust cell padding
                      pb: "8px", // Adjust cell padding
                      float: "left",
                      borderBottom: "unset",
                      color: "grey",
                    }}
                  >
                    {allUserLocks.length} Locks
                  </TableCell>
                  <TableCell
                    sx={{
                      fontSize: "14px",
                      pt: "0px", // Adjust cell padding
                      pl: "8px", // Adjust cell padding
                      pr: "8px", // Adjust cell padding
                      pb: "8px", // Adjust cell padding
                      float: "right",
                      borderBottom: "unset",
                      color: "grey",
                    }}
                  >
                    Unlock Date
                  </TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {locksToShow.map((address, index) => (
                  <TableRow
                    key={index}
                    sx={{
                      "& > *": {
                        borderBottom: "unset",
                        float: "left",
                      },
                    }}
                  >
                    <TableCell
                      sx={{
                        fontSize: "12px",
                        padding: "8px",
                        textAlign: "center",
                        borderBottom: "unset",
                      }}
                    >
                      {address.slice(0, 11) + "..." + address.slice(-11)}
                    </TableCell>

                    <TableCell
                      sx={{
                        fontSize: "12px",
                        padding: "8px",
                        float: "right",
                        borderBottom: "unset",
                      }}
                    >
                      {unlockTimesToShow[index]}
                    </TableCell>
                  </TableRow>
                ))}
              </TableBody>
            </Table>
          </TableContainer>
          <Pagination
            count={totalPages}
            page={page}
            onChange={handlePageChange}
            showFirstButton
            showLastButton
            sx={{ float: "left" }}
          />
        </React.Fragment>
      ) : (
        <Typography>No locks found for connected address.</Typography>
      )}
    </Box>
  );
}