import React, { useState, useEffect, useContext } from "react";

// MUI imports
import Box from "@mui/material/Box";
import Stepper from "@mui/material/Stepper";
import Step from "@mui/material/Step";
import StepLabel from "@mui/material/StepLabel";
import StepContent from "@mui/material/StepContent";
import Button from "@mui/material/Button";
import Typography from "@mui/material/Typography";
import CircularProgress from "@mui/material/CircularProgress";
import TextField from "@mui/material/TextField";
import ContentPasteIcon from "@mui/icons-material/ContentPaste";
import IconButton from "@mui/material/IconButton";

// import context
import { ProtocolContext } from "../../context/ProtocolContext.js";

export default function VerticalLinearStepper() {
  // import from context
  const {
    currentAccount,
    connectWallet,
    generateVault,
    genFormParams,
    setGenFormParams,
    contractLoading,
    contractLoaded,
    lockContractAddress,
  } = useContext(ProtocolContext);
  const [loading, setLoading] = useState(false);
  const [activeStep, setActiveStep] = useState(0);
  const [_unlockDate, setUnlockDate] = useState("");

  const handleNext = () => {
    setActiveStep((prevActiveStep) => prevActiveStep + 1);
  };

  const handleBack = () => {
    setActiveStep((prevActiveStep) => prevActiveStep - 1);
  };

  const handleReset = () => {
    setActiveStep(0);
  };

  // handle generate button     //if (!_owner, !_unlockDate) return // form vailidation later
  const handleSubmit = async (e) => {
    try {
      const { _owner, _unlockDate } = genFormParams;
      e.preventDefault();
      await generateVault();
    } catch (error) {
      console.error("generate error:", error);
    }
  };

  return (
    <Box sx={{ maxWidth: 400 }}>
      <Stepper activeStep={activeStep} orientation="vertical">
        <Step>
          <StepLabel>Connect Wallet</StepLabel>
          <StepContent>
            <Box>
              <Typography sx={{ float: "left" }}>
                Connect Wallet & Select Chain
              </Typography>
            </Box>
            <Box sx={{ mb: 2 }}>
              <div>
                {activeStep === 0 ? (
                  <>
                    {currentAccount ? (
                      <Button
                        variant="contained"
                        onClick={handleNext}
                        sx={{
                          mt: 1,
                          mr: 1,
                          float: "left",
                          mt: "5px",
                          minWidth: "225px",
                          maxWidth: "225px",
                        }}
                      >
                        Continue
                      </Button>
                    ) : (
                      <Button
                        variant="outlined"
                        sx={{
                          float: "left",
                          minWidth: "225px",
                          maxWidth: "225px",
                          mt: "15px",
                        }}
                        disabled={loading}
                        onClick={() => {
                          setLoading(true);
                          connectWallet();
                        }}
                      >
                        <Typography
                          sx={{ textTransform: "none", fontWeight: "600" }}
                        >
                          {loading ? (
                            <CircularProgress size={15} />
                          ) : (
                            "Connect Wallet"
                          )}
                        </Typography>
                      </Button>
                    )}
                  </>
                ) : activeStep === 2 ? (
                  <Button
                    variant="contained"
                    onClick={handleNext}
                    sx={{ mt: 1, mr: 1, float: "left", mt: "15px" }}
                  >
                    Finish
                  </Button>
                ) : (
                  <Button
                    variant="contained"
                    onClick={handleSubmit}
                    sx={{
                      mr: 1,
                      float: "left",
                      mt: "15px",
                      minWidth: "175px",
                      maxWidth: "175px",
                      minHeight: "36.5px",
                      maxHeight: "36.5px",
                    }}
                    disabled={contractLoading}
                  >
                    {contractLoading ? (
                      <CircularProgress size={15} />
                    ) : (
                      "Create Lock"
                    )}
                  </Button>
                )}
              </div>
            </Box>
          </StepContent>
        </Step>
        <Step>
          <StepLabel>Create Lock</StepLabel>
          <StepContent>
            <Box>
              <TextField
                id="filled-basic"
                size="small"
                label="Lock Length in Days"
                variant="filled"
                disabled={contractLoaded}
                onChange={(e) => {
                  setGenFormParams((prev) => {
                    return Object.assign({}, prev, {
                      _unlockDate: e.target.value,
                    });
                  });
                }}
                sx={{ float: "left", width: "80%" }}
              />

              {contractLoaded ? (
                <Button
                  variant="contained"
                  onClick={handleNext}
                  sx={{
                    mt: 1,
                    mr: 1,
                    float: "left",
                    mt: "15px",
                    minWidth: "175px",
                    maxWidth: "175px",
                    minHeight: "36.5px",
                    maxHeight: "36.5px",
                  }}
                  disabled={contractLoading}
                >
                  {contractLoading ? (
                    <CircularProgress size={15} />
                  ) : (
                    "Next Step"
                  )}
                </Button>
              ) : (
                <Button
                  variant="contained"
                  onClick={
                    (e) => handleSubmit(e)
                  }
                  sx={{
                    mt: 1,
                    mr: 1,
                    float: "left",
                    mt: "15px",
                    minWidth: "175px",
                    maxWidth: "175px",
                    minHeight: "36.5px",
                    maxHeight: "36.5px",
                  }}
                  disabled={contractLoading}
                >
                  {contractLoading ? (
                    <CircularProgress size={15} />
                  ) : (
                    "Create Lock"
                  )}
                </Button>
              )}
            </Box>
            <Box sx={{ mb: 2 }}>
              <div>
                {activeStep === 1 && (
                  <Button
                    variant="outlined"
                    onClick={handleBack}
                    sx={{ mt: 1, mr: 1, float: "left", mt: "15px" }}
                  >
                    Back
                  </Button>
                )}
              </div>
            </Box>
          </StepContent>
        </Step>

        <Step>
          <StepLabel>Deposit</StepLabel>
          <StepContent>
            <Typography sx={{ pb: "10px", float: "left" }}>
              Deposit To This Address
            </Typography>
            <Box sx={{ float: "left" }}>
              <b sx={{ color: "black", float: "left" }}>
                {lockContractAddress != null
                  ? lockContractAddress.slice(0, 10) +
                    "..." +
                    lockContractAddress.slice(-10)
                  : ""}
              </b>{" "}
              <IconButton size="small">
                <ContentPasteIcon
                  onClick={() => {
                    navigator.clipboard.writeText(lockContractAddress);
                  }}
                  sx={{
                    color: "#32CD32",
                    textDecoration: "none",
                    fontSize: "15px",
                    cursor: "pointer",
                  }}
                />{" "}
              </IconButton>
              <div>
                {activeStep === 2 && (
                  <>
                    <Button
                      variant="contained"
                      sx={{
                        mt: 1,
                        mr: 1,
                        float: "left",
                        mt: "15px",
                        minWidth: "115px",
                        maxWidth: "115px",
                        minHeight: "36.5px",
                        maxHeight: "36.5px",
                      }}
                    >
                      Share
                    </Button>
                    <Button
                      variant="outlined"
                      onClick={handleBack}
                      sx={{
                        mt: 1,
                        mr: 1,
                        float: "left",
                        mt: "15px",
                        minWidth: "105px",
                        maxWidth: "105px",
                        minHeight: "36.5px",
                        maxHeight: "36.5px",
                      }}
                    >
                      Back
                    </Button>
                  </>
                )}
              </div>
            </Box>
          </StepContent>
        </Step>
      </Stepper>
    </Box>
  );
}