import React, { useState, useEffect, useContext } from "react";

// import mui styling
import AppBar from "@mui/material/AppBar";
import Box from "@mui/material/Box";
import Toolbar from "@mui/material/Toolbar";
import Typography from "@mui/material/Typography";
import Button from "@mui/material/Button";
import { Container, Hidden } from "@mui/material";
import { styled } from "@mui/material/styles";
import Stack from "@mui/material/Stack";

import Modal from "@mui/material/Modal";

import { ProtocolContext } from "../context/ProtocolContext";

import ChainModal from "./Modals/ChainModal";

// chain image imports header
import Evmos from "../assets/images/Evmos_Chain.svg";
import Dogechain from "../assets/images/dogechain.png";
import Pulse from "../assets/images/pulse.png";
import bnb from "../assets/images/bnb.svg";
import ftm from "../assets/images/ftm.svg";
import avax from "../assets/images/avax.svg";
import poly from "../assets/images/poly.svg";
import iotex from "../assets/images/iotex.svg";
import one from "../assets/images/one.svg";
import eth from "../assets/images/eth.png";
import aur from "../assets/images/aurora.png";
import arb from "../assets/images/arbitrum.png";

const Homecomponent = styled("div")(({ theme }) => ({
  "& .btnlft": {
    marginLeft: "auto",
  },
  "& .MuiButton-outlinedInherit": {
    border: "1px solid rgba(241, 107, 69, 0.5) ",
    textTransform: "capitalize",
  },
  "& .MuiButton-outlinedInherit:hover": {
    color: "#f16b45 ",
  },
}));

// modal styling
const styleModal = {
  position: "absolute",
  top: "38%",
  left: "50%",
  transform: "translate(-50%, -50%)",
  width: 400,
  height: 335,
  bgcolor: "background.paper",
  border: "none",
  borderRadius: "25px",
  boxShadow: 30,
  p: 3,
};

export default function ButtonAppBar() {
  const {
    selectedChain,
    setSelectedChain,
    connectWallet,
    currentAccount,
    openChains,
    setOpenChains,
    handleOpenChainlist,
    handleCloseChainlist,
  } = useContext(ProtocolContext); // need to make a switch for chain name  handleCloseChainlist, handleOpenChainlist
  const [userName, setUserName] = useState();

  // imgs
  const logosHeaderEnums = {
    eth: <img style={{ width: 25, height: 25 }} src={eth} />,
    evm: <img style={{ width: 25, height: 25 }} src={Evmos} />,
    dgc: <img style={{ width: 25, height: 25 }} src={Dogechain} />,
    pls: <img style={{ width: 25, height: 25 }} src={Pulse} />,
    bnb: <img style={{ width: 25, height: 25 }} src={bnb} alt="b" />,
    ftm: <img style={{ width: 25, height: 25 }} src={ftm} alt="f" />,
    avax: <img style={{ width: 25, height: 25 }} src={avax} alt="a" />,
    poly: <img style={{ width: 25, height: 25 }} src={poly} alt="p" />,
    iotex: <img style={{ width: 25, height: 25 }} src={iotex} alt="i" />,
    one: <img style={{ width: 25, height: 25 }} src={one} alt="o" />,
    aurora: <img style={{ width: 25, height: 25 }} src={aur} alt="a" />,
    arb: <img style={{ width: 25, height: 25 }} src={arb} alt="a" />,
  };

  // chain select
  const [open, setOpen] = React.useState(false);

  const handleClose = () => {
    setOpen(false);
  };

  useEffect(() => {
    const data = window.localStorage.getItem("LOCAL_CHAIN_SELECTED");
    setSelectedChain(JSON.parse(data));
  }, []);

  useEffect(() => {
    if (selectedChain != null) {
      window.localStorage.setItem(
        "LOCAL_CHAIN_SELECTED",
        JSON.stringify(selectedChain),
      ); //cannot parse json objects or reference?
    }
  }, [selectedChain]);

  useEffect(() => {
    if (currentAccount != null)
      setUserName(
        `${currentAccount.slice(0, 6)}...${currentAccount.slice(38)}`,
      );
  }, [currentAccount]);

  useEffect(() => {
    if (currentAccount != null)
      setUserName(
        `${currentAccount.slice(0, 6)}...${currentAccount.slice(38)}`,
      );
  }, [currentAccount]);

  return (
    <Homecomponent>
      <Box sx={{ flexGrow: 1 }}>
        <AppBar
          position="static"
          sx={{ background: "transparent", boxShadow: "none" }}
        >
          <Container>
            <Toolbar>
              <Typography sx={{ fontWeight: "bold" }}>🔒 TokenLock</Typography>

              <Hidden mdDown>
                <div className="btnlft">
                  <Stack direction="row" spacing={2}>
                    {/* chain id */}
                    <Button
                      variant="outlined"
                      onClick={() => handleOpenChainlist()}
                    >
                      {selectedChain
                        ? logosHeaderEnums[selectedChain.img]
                        : null}
                    </Button>

                    <Box sx={{}}>
                      {currentAccount ? (
                        <Button onClick={null}>
                          <Box>
                            <b>{userName}</b>
                          </Box>
                        </Button>
                      ) : (
                        <Button onClick={() => connectWallet()}>
                          <Box />
                          <Typography
                            sx={{ textTransform: "none", fontWeight: "600" }}
                          >
                            Connect Wallet
                          </Typography>
                        </Button>
                      )}
                    </Box>
                  </Stack>
                </div>
              </Hidden>
            </Toolbar>

            <Modal open={openChains} onClose={handleCloseChainlist}>
              <Box sx={styleModal}>
                <ChainModal />
              </Box>
            </Modal>
          </Container>
        </AppBar>
      </Box>
    </Homecomponent>
  );
}
