import React, { useState, useEffect } from "react";
import { ethers } from "ethers";
import { networks } from "../lib/networks";
import Jate from 'jates'

// static ABI constants
import {
  TimeLockFactoryABI,
  TimeLockedWalletABI,
  //ExampleABI, etc...
} from "../lib/constants";

export const ProtocolContext = React.createContext();

let eth;

if (typeof window !== "undefined") {
  eth = window.ethereum;
}

const changeNetwork = async ({ networkName, setError }) => {
  try {
    if (!window.ethereum) throw new Error("MetaMask Not found - Please install metamask");
    await window.ethereum.request({
      method: "wallet_addEthereumChain",
      params: [
        {
          ...networks[networkName]
        }
      ]
    });
  } catch (err) {
    setError(err.message);
  }
};


// form GENERATE params
type GenFormParams = {
  _unlockDate: Number,
};

// Contract Interaction Code
export const ProtocolProvider = ({ children }) => {

// network change
const [error, setError] = useState();

const handleNetworkSwitch = async (networkName) => {
  setError();
  await changeNetwork({ networkName, setError });
};

const networkChanged = (chainId) => {
  console.log({ chainId });
};

const [open, setOpen] = useState(false); // modal set
const handleOpen = () => setOpen(true); // send modal
const handleClose = () => setOpen(false);// send modal close
const [openChains, setOpenChains] = useState(false); // chain list modal
const handleCloseChainlist = () => setOpenChains(false);// handle close for chain list
const handleOpenChainlist = () => setOpenChains(true);// handle close for chain list

const [currentAccount, setCurrentAccount] = useState(); // current account
const [selectedAddress, setSelectedAddress] = useState(""); // selected wallet to unlock
const [selectedChain, setSelectedChain] = useState();
const [currentChainNumber, setCurrentChainNumber] = useState();
const [totalSupply, setTotalSupply] = useState();
const [contractLoading , setContractLoading] = useState(false);
const [contractLoaded, setContractLoaded] = useState(false);
const [lockContractAddress, setLockContractAddress] = useState(""); // users lock address
const [allUserLocks, setAllUserLocks] = useState([]); // list of all connected accounts locks
const [unlockTimes, setUnlockTimes] = useState([]);

// network switch handling
useEffect(() => {
  window.ethereum.on("chainChanged", networkChanged);
  return () => {
    window.ethereum.removeListener("chainChanged", networkChanged);
  };
}, []);

useEffect(() => {
  checkIfWalletIsConnected();
  setCurrentChainNumber(window.ethereum.networkVersion);
}, []);

useEffect(() => {
  window.ethereum.on("accountsChanged", async(accounts) => {
    checkIfWalletIsConnected()
    setCurrentChainNumber(window.ethereum.networkVersion);
  })
}, [])

// change network
useEffect(() => {
  if(selectedChain != null){
  handleNetworkSwitch(selectedChain.name);
  setCurrentChainNumber(window.ethereum.networkVersion);
  }
}, [selectedChain])


useEffect(() => {

  const addCustomChain = async () => {

    try {
      await window.ethereum.request({
        method: 'wallet_addEthereumChain',
        params: [{
          chainId: networks.selectedChain.name.chainId,
          rpcUrl: networks.selectedChain.name.rpcUrls[0],
          chainName: networks.selectedChain.name.chainName,
          nativeCurrency: {
            name: 'Custom ETH',
            symbol: 'cETH',
            decimals: 18,
          },
          blockExplorerUrls: networks.selectedChain.name.blockExplorerUrls[0]
        }]
      });
    } catch (error) {
      console.error('Error adding custom chain:', error);
    }
  };

  addCustomChain();
}, [selectedChain]);



// all supported chains
const chainNamesAll = [

  // Testnets
  { name: 'goerli', img: 'eth', address: '0xB3F1D5E6DcAA54E97869e072feF20110A22bd24D'},
  { name: 'arbitrumGoerli', img: 'arb', address: '0x3E27c9470f3CDF13e433ED039cdc1F92d5B797D2'},
  { name: 'binanceSmartChain', img: 'bnb', address: ''},
  { name: 'fantom', img: 'ftm', address: ''},
  { name: 'evmos', img: 'evm', address: ''},
  // etc..

  // Main nets comming soon
  
]

// check wallet connect
const checkIfWalletIsConnected = async (metamask = eth) => {
  try {
    if (!metamask) return alert("please install metamask");
    const accounts = await metamask.request({ method: "eth_accounts" });
    if (accounts.length) {
      setCurrentAccount(accounts[0]);
      setCurrentChainNumber(window.ethereum.networkVersion);
    } else {
      setCurrentAccount(null)
    }
  } catch (error) {
    console.error(error);
    throw new Error("no ethereum object");
  }
};

// passing gen form variables
const [genFormParams: GenFormParams, setGenFormParams] = useState({
  // _owner: "",
  _unlockDate: "",
});

// main lock smart contract
const getEthereumContractLockFactory = () => {
  const provider = new ethers.providers.Web3Provider(eth);
  const signer = provider.getSigner();
  const lockInstance = new ethers.Contract(
    selectedChain.address,
    TimeLockFactoryABI,
    signer
  );
  return lockInstance;
};

// connect wallet
const connectWallet = async (metamask = eth) => {
  try {
    if (!metamask) return alert("please install metamask");
    const accounts = await metamask.request({
      method: "eth_requestAccounts",
    });
    setCurrentAccount(accounts[0]);
    handleNetworkSwitch(selectedChain.name);
    setCurrentChainNumber(window.ethereum.networkVersion);
  } catch (error) {
    console.error(error);
    throw new Error("no ethereum object");
  }
};

// generate a new vault / create new lock
const generateVault = async () => {

  const { _owner, _unlockDate } = genFormParams;

  const tokenlockInstance = getEthereumContractLockFactory();
  const date = new Jate()
  date.add(Number(_unlockDate), 'days')
  const daysToUnix = Math.round(date.getTime() / 1000)
  try {
    (async () => {
    setContractLoading(true);
    const generateResult = await tokenlockInstance.createTimeLockedWallet(
      "",
      daysToUnix,
    );
    let tx = await generateResult.wait();
    // tx loaded
    setContractLoading(false);
    setContractLoaded(true);
    setLockContractAddress(tx.logs[0].address.toString())
    })();
  } catch (error) {
    console.log(error);
  };
};


// manage locks area & get users vaults
const getLockInfo = async (connectedAccount = currentAccount) => {

  const tokenFactoryInstance = getEthereumContractLockFactory();
  const ethAddress = ethers.utils.getAddress(connectedAccount);

  try {

    const getAllUserVaults = await tokenFactoryInstance.getWallets(ethAddress);

    await setAllUserLocks(getAllUserVaults);

    const promises = allUserLocks.map(async (lockAddress) => {

        const provider = new ethers.providers.Web3Provider(eth);
        const signer = provider.getSigner();
        const walletlockInstance = new ethers.Contract(
          lockAddress,
          TimeLockedWalletABI,
          signer
        );
        const checkVaultInfo = await walletlockInstance.info();
        return checkVaultInfo;
    });

    // Execute all the promises concurrently
    const allVaultInfo = await Promise.all(promises);

    function convertToNumber(bigNumberObject) {
      return bigNumberObject.toNumber();
    }
    
    // Extract _hex values at index 3 from each inner array and save them in a new array
    const hexValuesArray = allVaultInfo.map((innerArray) => {
      const bigNumberObject = innerArray[3];
      return convertToNumber(bigNumberObject);
    });

    const humanReadableDates = hexValuesArray.map(timestamp => new Date(timestamp * 1000).toLocaleDateString());

    setUnlockTimes(humanReadableDates);

  } catch (error) {
    console.log(error);
  }
};

  return (
    <ProtocolContext.Provider
      value = {{
        currentAccount,
        connectWallet,
        setSelectedAddress,
        selectedAddress,
        handleOpen,
        handleClose,
        open,
        handleCloseChainlist,
        handleOpenChainlist,
        openChains,
        selectedChain,
        setSelectedChain,
        chainNamesAll,
        currentChainNumber,
        totalSupply,
        generateVault,
        genFormParams,
        setGenFormParams,
        contractLoading,
        contractLoaded,
        lockContractAddress,
        allUserLocks,
        getLockInfo,
        unlockTimes,

      }}
      >
        {children}
    </ProtocolContext.Provider>
  )
}