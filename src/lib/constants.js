// abi and contracts by chain
import TimeLockFactory_abi from './TimeLockWalletFactory.json'
import TimeLockedWallet_abi from './TimeLockedWallet.json'

// abi files
export const TimeLockFactoryABI = TimeLockFactory_abi.abi
export const TimeLockedWalletABI = TimeLockedWallet_abi.abi

// testnet factory contracts by chain
export const TimeLockFactoryGoerli = "0xB3F1D5E6DcAA54E97869e072feF20110A22bd24D"
//export const TimeLockFactoryArbitrumGoerli = "0x3E27c9470f3CDF13e433ED039cdc1F92d5B797D2"
//export const TimeLockFactoryEvmos = ""
//export const TimeLockFactoryBinance = ""
//export const TimeLockFactoryFantom = ""