import React, { Component } from "react";
import ResponsiveAppBar from "../components/NavBar";
import Core from "../components/Core";
import Footer from "../components/Footer";

import Layout from "../components/Layout";

export default class Home extends Component {
	render() {
		return (
			<div>
				<Layout>
				<ResponsiveAppBar />
                <Core />
				<Footer />
				</Layout>
			</div>
		);
	}
}
